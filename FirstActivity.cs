using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Locations;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;


namespace HelloMultiScreen
{
    [Activity (Label = "HelloMultiScreen", MainLauncher = true)]
    public class FirstActivity : Activity
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bundle"></param>
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Load the UI created in Main.axml          
            SetContentView(Resource.Layout.Main);

            // Get a reference to the button
            var showSecond = FindViewById<Button>(Resource.Id.showSecond);
            var startCall = FindViewById<Button>(Resource.Id.startCall);
            var locateMe = FindViewById<Button>(Resource.Id.locateMe);

            // You can use either this short form of StartActivity, which will create 
            // an intent internally, or the long form shown below.           
            //            showSecond.Click += (sender, e) => {           
            //                StartActivity (typeof(SecondActivity));
            //            };

            // Long form of StartActivity with the intent created in code so that
            // data can be added to the message payload using the PutExtra call.          
            showSecond.Click += (sender, e) =>
            {
                var second = new Intent(this, typeof(SecondActivity));
                second.PutExtra("FirstData", "Data from FirstActivity");
                StartActivity(second);
            };

            startCall.Click += (sender, e) =>
            {
                Intent callIntent = new Intent(Intent.ActionCall);
                callIntent.SetData(Android.Net.Uri.Parse("tel:" + "+498935870223"));
                StartActivity(callIntent);
            };

            locateMe.Click += (sender, e) =>
            {
                Intent navigationintent = new Intent(Intent.ActionView);
                navigationintent.SetData(Android.Net.Uri.Parse("http://maps.google.com/maps?daddr=48.13513,11.581981"));
                StartActivity(navigationintent);
            };


        }
    }
}